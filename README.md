Flutter App

A menu app for a small Restaurant.

Learning:
- Navigation
- Passing data via Navigation
- Gradient
- GridView
- Drawer and Tabbar
- Responsive
- Theme