import 'package:flutter/material.dart';

import './models/category.dart';

const DUMMY_CATEGORIES = const [
  Category(id: 'c1', title: 'Italian', color: Colors.purple),
  Category(id: 'c2', title: 'Quick and easy', color: Colors.redAccent),
  Category(id: 'c3', title: 'Hamburger', color: Colors.orange),
  Category(id: 'c4', title: 'German', color: Colors.amber),
  Category(id: 'c5', title: 'Vienamese', color: Colors.red),
  Category(id: 'c6', title: 'French', color: Colors.green),
  Category(id: 'c7', title: 'Singapore', color: Colors.lightGreen),
  Category(id: 'c8', title: 'Malaysian', color: Colors.pink),
  Category(id: 'c9', title: 'Japan', color: Colors.lightBlue),
  Category(id: 'c10', title: 'China', color: Colors.teal),
];
